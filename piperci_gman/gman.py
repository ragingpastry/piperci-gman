import datetime
import re

from flask import request
from peewee import DoesNotExist
from piperci_gman.marshaller import MarshalError, Marshaller
from piperci_gman.orm.models import (Artifact, Task, TaskEvent,
                                     TaskEventSchema, TaskSchema,
                                     QueryFailed, ZeroResults)
from piperci_gman.resource import PiperCiResource
from werkzeug.exceptions import BadRequest
from marshmallow import ValidationError


class GManMarshaller(Marshaller):

    def __init__(self, raw):
        super(GManMarshaller, self).__init__(raw)

        self._task = None
        self._event = None

    def enforce(self, context):  # noqa: C901

        if context == 'create_task':
            try:
                self._task = TaskSchema().load(self.raw_data)
            except ValidationError as e:
                self.errors.extend(e.messages,
                                   data=self.raw_data)
            try:
                self._event = TaskEventSchema().load(self.raw_data,
                                                     partial=('timestamp',))
                self._event.timestamp = datetime.datetime.now()
            except ValidationError as e:
                self.errors.extend(e.messages,
                                   data=self.raw_data)

            disallowed = ('task_id', 'timestamp')

            if self.raw_data.get('status', '') not in ('started', 'received'):
                self.errors.add('status',
                                "Task creation must be 'started' or 'received'")
            else:
                if (self.raw_data.get('status') == 'received'
                        and not len(self.raw_data.get('thread_id', ''))):
                    self.errors.add('thread_id',
                                    'Thread_id is required for status received')
                if (self.raw_data.get('status') == 'received'
                        and not len(self.raw_data.get('parent_id', ''))):
                    self.errors.add('parent_id',
                                    'Parent_id is required for status received')

                if (self.raw_data.get('status', '') == 'started'
                        and not len(self.raw_data.get('thread_id', ''))):
                    self._task.thread_id = self._task.task_id

        elif context == 'add_event':
            disallowed = ('caller', 'thread_id', 'timestamp', 'project', 'run_id')

            try:
                self._event = TaskEventSchema().load(self.raw_data,
                                                     partial=('timestamp',))

                self._event.timestamp = datetime.datetime.now()
            except ValidationError as e:
                self.errors.extend(e.messages,
                                   data=self.raw_data)

            if self.raw_data.get('status', '') in ('started', 'received'):
                self.errors.add('status',
                                "Updates may not be value 'started' or 'received'")

        for key in disallowed:
            if key in self.raw_data:
                self.errors.add(key, f'May not be specified for {context}')

        if len(self.errors.errors):
            raise MarshalError(self.errors)

    @property
    def task(self):
        return self._task

    @property
    def event(self):
        return self._event


class GManResource(PiperCiResource):

    def head_event_states(self, task_events):
        states = self.task_states(task_events)
        headers = {'x-gman-tasks-running': len(states['running']),
                   'x-gman-tasks-completed': len(states['completed']),
                   'x-gman-tasks-pending': len(states['pending']),
                   'x-gman-tasks-failed': len(states['failed'])}
        return None, 200, headers


class TaskResource(GManResource):

    def get(self, task_id, events=None):
        try:
            if events:
                return TaskEventSchema(many=True) \
                    .dump(self.task_events(task_id))
            else:
                return TaskSchema().dump(self.task(task_id))

        except (ZeroResults, DoesNotExist):
            return self.NotFound()

    def head(self, task_id, events=None):

        try:
            task_events = self.task_events(task_id)

            if events:
                return None, 200, {'x-gman-events': len(task_events)}
            else:
                state = [k for k, v in self.task_states(task_events).items()
                         if len(v)][0]
                return None, 200, {'x-gman-task-state': state}

        except ZeroResults:
            if events:
                return None, 404, {'x-gman-events': 0}
            else:
                return None, 404, {'x-gman-task-state': 'not found'}

    def post(self, task_id=None, return_resource=False):  # noqa: C901
        try:
            if task_id:
                raise BadRequest('Invalid args for this method')
            raw = request.get_json(force=True)
            return TaskEventSchema().dump(self.create_task(data=raw))
        except MarshalError as e:
            return e.errors.emit(), 422
        except BadRequest as e:
            return self.BadRequest(str(e))
        except Exception as e:
            return self.InternalError(str(e))

    def put(self, task_id, events=None):  # noqa: C901
        try:
            if events:
                raise BadRequest('Events may not be specified on a PUT')
            raw = request.get_json(force=True)
            return TaskEventSchema().dump(self.create_event(task_id, data=raw))

        except MarshalError as e:
            return e.errors.emit(), 422
        except DoesNotExist:
            return self.NotFound()
        except BadRequest as e:
            return self.BadRequest(str(e))
        except Exception as e:
            return self.InternalError(str(e))

    def create_event(self, task_id, data):
        marshaller = GManMarshaller(data)
        marshaller.enforce('add_event')
        task = Task.get(Task.task_id == task_id)

        try:
            completed = self.task_completed_event(task_id)
            marshaller.errors.add('status',
                                  f'A closing event for {completed.task_id} of '
                                  f'{completed.status} already exists.')
            raise MarshalError(marshaller.errors)
        except ZeroResults:
            pass

        if marshaller.event.artifact:
            Artifact.get(Artifact.artifact_id == marshaller.event.artifact)

        event = TaskEvent.create(task=task,
                                 message=marshaller.event.message,
                                 status=marshaller.event.status,
                                 timestamp=marshaller.event.timestamp,
                                 artifact=marshaller.event.artifact)

        if event:
            return event
        else:
            raise QueryFailed(f'Event Creation Error for task_id {task_id}')

    def create_task(self, data):
        try:
            marshaller = GManMarshaller(data)
            marshaller.enforce('create_task')

            if marshaller.task.thread_id != marshaller.task.task_id:
                Task.get(Task.task_id == marshaller.task.thread_id)

            if marshaller.task.parent_id:
                Task.get(Task.task_id == marshaller.task.parent_id)

            if marshaller.event.artifact:
                Artifact.get(Artifact.artifact_id == marshaller.event.artifact)

            task = Task.create(task_id=marshaller.task.task_id,
                               run_id=marshaller.task.run_id,
                               project=marshaller.task.project,
                               thread_id=marshaller.task.thread_id,
                               parent_id=marshaller.task.parent_id,
                               caller=marshaller.task.caller)

            event = TaskEvent.create(task=task,
                                     message=marshaller.event.message,
                                     status=marshaller.event.status,
                                     timestamp=marshaller.event.timestamp,
                                     artifact=marshaller.event.artifact)

            return event
        except DoesNotExist:
            marshaller.errors.add('missing_task',
                                  'An existing task_id must exist'
                                  ' for thread_id and parent_id')
            raise MarshalError(marshaller.errors)
        except AttributeError as e:
            reg = re.compile(r" '(.+)'")
            matches = reg.search(str(e))
            marshaller.errors.add(matches.groups()[0], 'Is required but not valid')
            raise MarshalError(marshaller.errors)


class ThreadResource(GManResource):

    def get(self, thread_id, events=None):

        try:
            if events:
                return TaskEventSchema(many=True) \
                    .dump(self.task_event_thread(thread_id))
            else:
                return TaskSchema(many=True).dump(self.task_thread(thread_id))
        except (ZeroResults, DoesNotExist):
            return self.NotFound()

    def head(self, thread_id, events=None):

        try:
            task_events = self.task_event_thread(thread_id)
            return self.head_event_states(task_events)
        except ZeroResults:
            headers = {'x-gman-tasks-running': 0,
                       'x-gman-tasks-completed': 0,
                       'x-gman-tasks-pending': 0,
                       'x-gman-tasks-failed': 0}
            return None, 404, headers


class RunResource(GManResource):

    def get(self, run_id, events=None):

        try:
            if events:
                return TaskEventSchema(many=True) \
                    .dump(self.task_events_run_id(run_id))
            else:
                return TaskSchema(many=True).dump(self.tasks_run_id(run_id))
        except (ZeroResults, DoesNotExist):
            return self.NotFound()

    def head(self, run_id, events=None):
        try:
            task_events = self.task_events_run_id(run_id)
            return self.head_event_states(task_events)
        except ZeroResults:
            headers = {'x-gman-tasks-running': 0,
                       'x-gman-tasks-completed': 0,
                       'x-gman-tasks-pending': 0,
                       'x-gman-tasks-failed': 0}
            return None, 404, headers
