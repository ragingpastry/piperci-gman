import uuid

import pytest

import piperci_gman.gman as gman

from piperci_gman.orm.models import Task, TaskEvent


@pytest.fixture
def testthread(api, client, testtask):
    """Creates a thread with one parent task and one recieved task"""
    parent_task = testtask()
    parent_task_id = parent_task.json['task']['task_id']

    event = {
        'status': f'info',
        'message': f'Testing with status info',
    }
    resp = client.put(api.url_for(gman.TaskResource, task_id=parent_task_id), json=event)
    assert resp.status_code == 200, 'first part closing event with {status}=info'

    new_task = {
        'thread_id': parent_task_id,
        'message': 'adding an open task',
        'status': 'received',
        'run_id': parent_task.json['task']['run_id'],
        'parent_id': parent_task_id,
        'caller': 'pytest_next_task',
        'project': 'pytest'
    }

    # Delegate 3
    resp = client.put(api.url_for(gman.TaskResource, task_id=parent_task_id),
                      json={
        'status': f'delegated',
        'message': f'Delegated new task {resp.json["task"]["task_id"]}'})
    assert resp.status_code == 200, 'marking a delegated task'

    resp = client.put(api.url_for(gman.TaskResource, task_id=parent_task_id),
                      json={
        'status': f'delegated',
        'message': f'Delegated new task {resp.json["task"]["task_id"]}'})
    assert resp.status_code == 200, 'marking a delegated task'

    resp = client.put(api.url_for(gman.TaskResource, task_id=parent_task_id),
                      json={
        'status': f'delegated',
        'message': f'Delegated new task {resp.json["task"]["task_id"]}'})
    assert resp.status_code == 200, 'marking a delegated task'

    resp = client.put(api.url_for(gman.TaskResource, task_id=parent_task_id),
                      json={
        'status': f'delegated',
        'message': f'Delegated new task {resp.json["task"]["task_id"]}'})
    assert resp.status_code == 200, 'marking a delegated task'

    # Receive 2, complete 1, fail 1
    received_task = testtask(json=new_task)
    assert received_task.status_code == 200, 'failed to create delegated task'

    resp = client.put(api.url_for(gman.TaskResource, task_id=parent_task_id),
                      json={'status': f'completed',
                            'message': f'Testing with status completed'})
    assert resp.status_code == 200, 'first part closing event with status completed'

    received_task2 = testtask(json=new_task)
    assert received_task.status_code == 200, 'failed to create delegated task'

    resp = client.put(api.url_for(gman.TaskResource,
                                  task_id=received_task2.json['task']['task_id']),
                      json={
                          'status': f'failed',
                          'message': f'Testing with status failed'})
    assert resp.status_code == 200, 'first part closing event with status completed'

    return parent_task_id


def test_get_task(api, client, testtask):
    task_id = testtask().json['task']['task_id']

    resp = client.get(api.url_for(gman.TaskResource, task_id=task_id))

    assert resp.status_code == 200
    assert resp.json['task_id'] == task_id, 'Bad task_id returned'


def test_get_task_bad(api, client):
    resp = client.get(api.url_for(gman.TaskResource, task_id=uuid.uuid4()))

    assert resp.status_code == 404


def test_get_task_no_task_id(api, client, testtask):

    resp = client.get(api.url_for(gman.TaskResource))

    assert resp.status_code == 400


def test_get_task_events(api, client, testtask):
    task_id = testtask().json['task']['task_id']

    event = {
        'status': 'info',
        'message': f'Testing with status info',
    }

    client.put(api.url_for(gman.TaskResource, task_id=task_id), json=event)

    resp = client.get(api.url_for(gman.TaskResource, task_id=task_id, events='events'))

    assert resp.status_code == 200
    assert len(resp.json) == 2, 'Event count is off!'

    for event in resp.json:
        event['task']['task_id'] == task_id, 'Bad task_id returned'


def test_get_task_events_no_events(api, client, testtask):
    task_id = testtask().json['task']['task_id']

    task = Task.get(Task.task_id == task_id)

    TaskEvent.delete() \
             .where(TaskEvent.task == task) \
             .execute()

    events = TaskEvent.select() \
                      .join(Task) \
                      .where(Task.task_id == task_id)

    assert len([x for x in events]) == 0, 'events where not cleared out'

    resp = client.get(api.url_for(gman.TaskResource, task_id=task_id, events='events'))

    assert resp.status_code == 404


def test_head_task_id(api, client, testtask):

    task_id = testtask().json['task']['task_id']

    event = {
        'status': f'info',
        'message': f'Testing with status info',
    }

    resp = client.put(api.url_for(gman.TaskResource, task_id=task_id), json=event)
    assert resp.status_code == 200, 'first part closing event with {status}=failed'

    resp = client.head(f'/task/{task_id}')
    assert resp.headers['x-gman-task-state'] == 'running'


def test_head_task_no_task_id(api, client, testtask):

    resp = client.head(api.url_for(gman.TaskResource))

    assert resp.status_code == 400


def test_head_task_id_events(api, client, testtask):

    task_id = testtask().json['task']['task_id']

    event = {
        'status': f'info',
        'message': f'Testing with status info',
    }

    resp = client.put(api.url_for(gman.TaskResource, task_id=task_id), json=event)
    assert resp.status_code == 200, 'first part closing event with {status}=info'

    resp = client.head(f'/task/{task_id}/events')
    assert 'x-gman-events' in resp.headers
    assert int(resp.headers['x-gman-events']) == 2


def test_head_task_events_non_existing(api, client):
    resp = client.head(f'/task/9435b705-fbca-49a9-a4ab-400cc932bdd1/events')
    assert int(resp.headers['x-gman-events']) == 0


def test_head_task_non_existing(api, client):
    resp = client.head(f'/task/9435b705-fbca-49a9-a4ab-400cc932bdd1')
    assert resp.headers['x-gman-task-state'] == 'not found'


def test_head_gman_task(api, client, testtask):
    testtask()
    resp = client.head('/task')
    resp.status_code == 404


def test_get_thread(api, client, testthread):

    resp = client.get(f'/thread/{testthread}')

    assert resp.status_code == 200
    for task in resp.json:
        assert 'task_id' in task, 'not a valid task json response'


def test_get_thread_events(api, client, testthread):

    resp = client.get(f'/thread/{testthread}/events')

    assert resp.status_code == 200
    for event in resp.json:
        assert 'task' in event, 'not a valid task event, must have a task'
        assert isinstance(event['task'], dict), 'task must be a dict'


def test_get_thread_bad_id(api, client):
    resp = client.get('/thread/9435b705-fbca-49a9-a4ab-400cc932bdd1')
    assert resp.status_code == 404


def test_head_thread_events(api, client, testthread):
    resp = client.head(f'/thread/{testthread}')
    assert resp.status_code == 200
    assert 'x-gman-tasks-running' in resp.headers
    assert 'x-gman-tasks-completed' in resp.headers
    assert 'x-gman-tasks-failed' in resp.headers
    assert 'x-gman-tasks-pending' in resp.headers

    assert int(resp.headers['x-gman-tasks-completed']) == 1
    assert int(resp.headers['x-gman-tasks-running']) == 1
    assert int(resp.headers['x-gman-tasks-failed']) == 1
    assert int(resp.headers['x-gman-tasks-pending']) == 2


def test_head_thread_non_existing(api, client):
    resp = client.head('/thread/9435b705-fbca-49a9-a4ab-400cc932bdd1')
    resp.status_code == 404
    assert int(resp.headers['x-gman-tasks-completed']) == 0
    assert int(resp.headers['x-gman-tasks-running']) == 0
    assert int(resp.headers['x-gman-tasks-failed']) == 0
    assert int(resp.headers['x-gman-tasks-pending']) == 0


def test_get_by_run_id_events(api, client, testtask):
    task1 = testtask()
    testtask()

    resp = client.get(api.url_for(gman.RunResource,
                                  run_id=task1.json['task']['run_id'],
                                  events='events'))

    assert resp.status_code == 200
    assert len(resp.json) == 2
    assert 'task' in resp.json[0]
    for event in resp.json:
        assert event['task']['run_id'] == task1.json['task']['run_id'], ('Bad run_id'
                                                                         ' returned')


def test_get_by_run_id(api, client, testtask):
    task1 = testtask()
    testtask()

    resp = client.get(api.url_for(gman.RunResource,
                                  run_id=task1.json['task']['run_id']))
    assert resp.status_code == 200
    assert len(resp.json) == 2
    assert 'task_id' in resp.json[0]
    for task in resp.json:
        assert task['run_id'] == task1.json['task']['run_id'], 'Bad run_id returned'


def test_get_bad_run_id(api, client):
    resp = client.get(api.url_for(gman.RunResource, run_id='bad_run_id_22'))

    assert resp.status_code == 404


def test_head_bad_run_id(api, client):
    resp = client.head(api.url_for(gman.RunResource, run_id='bad_run_id_22'))

    assert resp.status_code == 404


def test_head_run_id_events(api, client, testthread):
    resp = client.get(f'/task/{testthread}')
    assert resp.status_code == 200
    assert 'run_id' in resp.json

    run_id = resp.json['run_id']

    resp = client.head(f'/run/{run_id}')

    assert resp.status_code == 200
    assert 'x-gman-tasks-running' in resp.headers
    assert 'x-gman-tasks-completed' in resp.headers
    assert 'x-gman-tasks-failed' in resp.headers
    assert 'x-gman-tasks-pending' in resp.headers

    assert int(resp.headers['x-gman-tasks-completed']) == 1
    assert int(resp.headers['x-gman-tasks-running']) == 1
    assert int(resp.headers['x-gman-tasks-failed']) == 1
    assert int(resp.headers['x-gman-tasks-pending']) == 2
