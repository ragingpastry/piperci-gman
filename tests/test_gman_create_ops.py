import uuid

import pytest

import piperci_gman.gman as gman

from piperci_gman.orm.models import TaskEvent, db

gman_task_create_post = [
    ({'run_id': '1',
      'project': 'pytest suite',
      'caller': 'test_case_1',
      'status': 'started',
      'message': 'test start with status == started'},
     200,
     [(lambda x: 'task_id' in x['task'], 'Missing "task_id"'),
      (lambda x: uuid.UUID(x['task']['task_id']), 'Invalid "task_id" UUID')]),
    ({'run_id': '2',
      'project': 'pytest suite',
      'caller': 'test_case_2',
      'message': 'test creation of a normal task no status'},
     422,
     [(lambda x: 'errors' in x, 'No error message returned on 422'),
      (lambda x: 'status' in x['errors'], 'Did not error on status')]),
    ({'run_id': '4',
      'project': 'pytest suite',
      'caller': 'test_case_4',
      'status': 'info',
      'message': 'try to create a function with "info" status'},
     422,
     [(lambda x: 'errors' in x, 'No error message returned'),
      (lambda x: 'status' in x['errors'],
       'Task allowed status other then started for a creation event')]),
    ({'run_id': '5',
      'task_id': 'ba279fdc-e11d-4bc8-828c-a44e35b55175',
      'project': 'pytest suite',
      'caller': 'test_case_5',
      'message': 'try to create task with a task_id',
      'status': 'started'},
     422,
     [(lambda x: 'errors' in x, 'No error message returned'),
      (lambda x: 'May not be specified for create_task' in str(x['errors']),
       'Task creation allowed a defined task_id')]),
    ({'run_id': '6',
      'status': 'started',
      'project': 'pytest suite',
      'caller': 'test_case_6',
      'thread_id': 'ba279fdc-e11d-4bc8-828c-a44e35b55175',
      'message': 'thread_id must be an existing task_id'},
     422,
     [(lambda x: 'An existing task_id must exist for thread_id'
       in str(x['errors']['missing_task']),
       'thread_id was allowed for not existing task')]),
    ({'run_id': '7',
      'project': '9435b705-fbca-49a9-a4ab-400cc932bdd1',
      'caller': 'test_case_7',
      'status': 'started',
      'message': 'Test project is a UUID'},
     200,
     [(lambda x: uuid.UUID(x['task']['project']), 'Invalid "project" UUID')]),
    ({'run_id': '8',
      'project': 'pytest suite',
      'caller': 'test_case_8',
      'status': 'started',
      'timestamp': '2019-05-15T15:52:05.719859+00:00',
      'message': 'test defining valid timestamp'},
     422,
     [(lambda x: 'errors' in x, 'No error message returned')]),
    ({'run_id': '9',
      'project': 'pytest suite',
      'caller': 'test_case_9',
      'status': 'started',
      'timestamp': '2019-dfdfd05-15T15:52:05.719859+00:00',
      'message': 'test defining invalid timestamp'},
     422,
     [(lambda x: 'errors' in x, 'No error message returned')]),
    ({'project': 'pytest suite',
      'caller': 'test_case_10',
      'status': 'started',
      'message': 'testing missing run_id'},
     422,
     [(lambda x: 'errors' in x, 'No error message returned')]),
    ({'project': 'pytest suite',
      'caller': 'test_case_11',
      'status': 'started',
      'return_code': 'bad_value',
      'run_id': '5',
      'message': 'testing invalid return_code'},
     422,
     [(lambda x: 'errors' in x, 'No error message returned'),
      (lambda x: 'return_code' in x['errors'], 'Return code check missing')]),
    ({'project': 'pytest suite',
      'caller': 'test_case_12',
      'status': 'started',
      'return_code': 21,
      'run_id': '5',
      'message': 'testing invalid return_code'},
     200,
     [(lambda x: 'return_code' in x, 'return_code not in task')])]


@pytest.mark.parametrize('data,resp_code,tests', gman_task_create_post)
def test_post(data, resp_code, tests, api, client):
    resp = client.post(api.url_for(gman.TaskResource), json=data)
    assert resp.status_code == resp_code, f'Invalid response code {resp_code}'
    if tests:
        for test in tests:
            assert test[0](resp.json), test[1]


def test_post_w_task_id(api, client):
    data = {'run_id': '1_post_task',
            'project': '9435b705-fbca-49a9-a4ab-400cc932bdd1',
            'caller': 'test_post_w_task_id',
            'status': 'started',
            'message': 'Test project is a UUID'}
    resp = client.post(api.url_for(gman.TaskResource,
                                   task_id='9435b705-fbca-49a9-a4ab-400cc932bdd1'),
                       json=data)

    assert resp.status_code == 400, f'Invalid response code {resp.status_code}'


def test_post_w_artifact(api, client, artifact):

    artifact_id = artifact()[0]['artifact_id']

    data = gman_task_create_post[0][0]
    data['artifact'] = artifact_id

    resp = client.post(api.url_for(gman.TaskResource), json=data)
    assert artifact_id == resp.json['artifact']
    assert resp.status_code == 200, 'Unable to create task with an artifact used event'


gman_post_delegated = [
    ({'run_id': '1',
      'project': 'pytest suite',
      'caller': 'test_post_delegated_1',
      'status': 'received',
      'message': 'test create with status == received no thread_id'},
     422,
     [(lambda x: 'Thread_id is required for status received'
                 in str(x['errors']['thread_id']),
                 'thread_id was not required for status received')]),
    ({'run_id': '2',
      'project': 'pytest suite',
      'caller': 'test_post_delegated_2',
      'status': 'received',
      'thread_id': '9435b705-fbca-49a9-a4ab-400cc932bdd1',
      'parent_id': '{}',
      'message': 'test create with status == received bad thread_id'},
     422,
     [(lambda x: 'An existing task_id must exist'
                 in str(x['errors']['missing_task']),
                 'thread_id was allowed to be created for a non-existing task')]),
    ({'run_id': '3',
      'project': 'pytest suite',
      'caller': 'test_post_delegated_3',
      'status': 'received',
      'thread_id': '{}',
      'parent_id': '{}',
      'message': 'test create with status == received good thread_id'},
     200,
     [(lambda x: 'errors' not in x, 'errors detected on expected good post')]),
    ({'run_id': '4',
      'project': 'pytest suite',
      'caller': 'test_post_delegated_4',
      'status': 'received',
      'thread_id': '{}',
      'parent_id': '9435b705-fbca-49a9-a4ab-400cc932bdd1',
      'message': 'test create with status == received bad parent_id'},
     422,
     [(lambda x: 'An existing task_id must exist'
                 in str(x['errors']['missing_task']),
                 'parent_id was allowed to be created for a non-existing task')]),
    ({'run_id': '5',
      'project': 'pytest suite',
      'caller': 'test_post_delegated_4',
      'status': 'received',
      'thread_id': '{}',
      'message': 'test create with status == received no parent_id'},
     422,
     [(lambda x: 'Parent_id is required for status received'
                 in str(x['errors']['parent_id']),
                 'Recieved was allowed without a parent_id')])]


@pytest.mark.parametrize('data,resp_code,tests', gman_post_delegated)
def test_post_delegated(data, resp_code, tests, api, client, testtask):
    thread_id = testtask().json['task']['task_id']

    if 'thread_id' in data and data['thread_id'] == '{}':
        data['thread_id'] = data['thread_id'].format(thread_id)

    if 'parent_id' in data and data['parent_id'] == '{}':
        data['parent_id'] = data['parent_id'].format(thread_id)

    resp = client.post(api.url_for(gman.TaskResource), json=data)

    assert resp.status_code == resp_code, f'Invalid response code {resp_code}'

    if tests and len(tests):
        for test in tests:
            assert test[0](resp.json), test[1]


def test_post_no_body(api, client):

    resp = client.post(api.url_for(gman.TaskResource))
    assert resp.status_code == 400


def test_failed_event_create_IDK(api, client, monkeypatch, testtask):
    task = testtask()

    def myfunc(*args, **kwargs):
        kwargs['uri'] = {'not a valid thing'}
        return None

    monkeypatch.setattr('piperci_gman.orm.models.TaskEvent.create', myfunc)

    resp = client.put(api.url_for(gman.TaskResource,
                                  task_id=task.json['task']['task_id']),
                      json={'message': 'test no table',
                            'status': 'info'})

    assert resp.status_code == 500


def test_failed_task_create_no_table(api, client, monkeypatch):

    db.drop_tables([TaskEvent])

    resp = client.post(api.url_for(gman.TaskResource),
                       json={'run_id': '1_post_task',
                             'project': '9435b705-fbca-49a9-a4ab-400cc932bdd1',
                             'caller': 'test_post_w_task_id',
                             'status': 'started',
                             'message': 'Test project is a UUID'})

    assert resp.status_code == 500


def test_failed_event_create_no_table(api, client, monkeypatch, testtask):
    task = testtask()

    db.drop_tables([TaskEvent])

    resp = client.put(api.url_for(gman.TaskResource,
                                  task_id=task.json['task']['task_id']),
                      json={'message': 'test no table',
                            'status': 'info'})
    assert resp.status_code == 500
