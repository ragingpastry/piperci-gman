import os
import pytest

import piperci_gman.artman as artman
import piperci_gman.gman as gman

from attrdict import AttrDict
from flask import current_app
from piperci_gman.app import app as papp
from piperci_gman.orm.models import db_init
from piperci_gman.util import Api


@pytest.fixture(autouse=True)
def db_file_notouchy():
    try:
        fattr = os.stat('gman.db')
    except FileNotFoundError:
        fattr = None

    yield None

    try:
        after_attr = os.stat('gman.db')
    except FileNotFoundError:
        after_attr = None

    assert fattr == after_attr, 'Something used the default db file during testing!'


@pytest.fixture
def db():
    db_config = AttrDict({'type': 'sqlite', 'uri': ':memory:'})
    return db_init(db_config)


@pytest.fixture
def app(db):  # required by pytest_flask
    return papp


@pytest.fixture
def api():
    return Api(current_app, catch_all_404s=True)


gman_task_create = {
    'run_id': 'create_1',
    'project': 'gman_test_data',
    'caller': 'test_case_create_1',
    'status': 'started',
    'message': 'a normal task creation body'
}


@pytest.fixture
def testtask(api, client):
    def create(json=None):
        json = json if json else gman_task_create
        return client.post(api.url_for(gman.TaskResource), json=json)
    return create


@pytest.fixture
def artifact(api, client, testtask):
    art_data = {'uri': 'https://someminio.example.com/art1',
                'sri': 'sha256-sCDaaxdshXhK4sA/v4dMHiMWhtGyQwA1fP8PgrN0O5g=',
                'sri-urlsafe':
                'c2hhMjU2LXNDRGFheGRzaFhoSzRzQS92NGRNSGlNV2h0R3lRd0ExZlA4UGdyTjBPNWc9',
                'type': 'artifact',
                'caller': 'pytest'}

    def create(data=None):
        if data:
            art_data.update(data)
            data = art_data
        else:
            data = art_data

        if 'task_id' not in data:
            task = testtask()
            data['task_id'] = task.json['task']['task_id']

        resp = client.post(api.url_for(artman.ArtifactResource), json=data)
        return resp.json, data

    return create


@pytest.fixture
def artifacts_data():
    return [
        {'uri': 'https://someminio.example.com/art1',
         'sri': 'sha256-sCDaaxdshXhK4sA/v4dMHiMWhtGyQwA1fP8PgrN0O5g=',
         'sri-urlsafe':
             'c2hhMjU2LXNDRGFheGRzaFhoSzRzQS92NGRNSGlNV2h0R3lRd0ExZlA4UGdyTjBPNWc9',
         'type': 'artifact',
         'caller': 'pytest'},
        {'uri': 'https://someminio.example.com/art2',
         'sri': 'sha256-jrT+J2yEC8wfUr6N/YxxbR/ux5y2GriIqXsySl5uVK8=',
         'sri-urlsafe':
             'c2hhMjU2LWpyVCtKMnlFQzh3ZlVyNk4vWXh4YlIvdXg1eTJHcmlJcVhzeVNsNXVWSzg9',
         'type': 'source',
         'caller': 'pytest'},
        {'uri': 'https://someminio.example.com/art1',
         'sri': 'sha256-sCDaaxdshXhK4sA/v4dMHiMWhtGyQwA1fP8PgrN0O5g=',
         'sri-urlsafe':
             'c2hhMjU2LXNDRGFheGRzaFhoSzRzQS92NGRNSGlNV2h0R3lRd0ExZlA4UGdyTjBPNWc9',
         'type': 'artifact',
         'caller': 'pytest',
         'task_id': 'New'}]


@pytest.fixture
def artifacts(api, client, testtask, artifacts_data):
    task = testtask()
    arts = []

    for artifact in artifacts_data:
        if 'task_id' in artifact and artifact['task_id'] == 'New':
            _task = testtask()
        else:
            _task = task

        data = {}
        data.update(artifact)
        data['task_id'] = _task.json['task']['task_id']

        resp = client.post(api.url_for(artman.ArtifactResource), json=data)
        if resp.status_code != 200:
            pytest.fail(str(resp.json) + str(data))
        arts.append((resp,
                     data))

    return arts
