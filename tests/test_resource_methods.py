import pytest

from piperci_gman.resource import PiperCiResource


class Task(object):

    def __init__(self, task_id, parent_id):
        self.task_id = task_id
        self.parent_id = parent_id


class Event(object):

    def __init__(self, status, task):
        self.status = status
        self.task = task


@pytest.fixture
def task_scenarios():

    parent_1 = Task('1', None)
    child_1 = Task('2', '1')

    # Normal flow | 2 complete, 0 running, 0 pending, 0 failed
    scenario_1 = ('Normal flow', [
        Event('started', parent_1),
        Event('info', parent_1),
        Event('delegated', parent_1),
        Event('received', child_1),
        Event('completed', parent_1),
        Event('info', child_1),
        Event('completed', child_1)
    ], {'completed': 2, 'running': 0, 'pending': 0, 'failed': 0})

    # No received | 1 complete, 0 running, 1 pending, 0 failed
    scenario_2 = ('No received', [
        Event('started', parent_1),
        Event('info', parent_1),
        Event('delegated', parent_1),
        Event('completed', parent_1),
        Event('info', child_1)
    ], {'completed': 1, 'running': 0, 'pending': 1, 'failed': 0})

    # Recevied before delegate all complete
    # | 2 complete, 0 running, 0 pending, 0 failed
    scenario_3 = ('Recevied before delegate all complete', [
        Event('started', parent_1),
        Event('info', parent_1),
        Event('received', child_1),
        Event('delegated', parent_1),
        Event('completed', parent_1),
        Event('info', child_1),
        Event('completed', child_1),
    ], {'completed': 2, 'running': 0, 'pending': 0, 'failed': 0})

    # No complete on received | 1 complete, 1 running, 0 pending, 0 failed
    scenario_4 = ('No complete on received', [
        Event('started', parent_1),
        Event('info', parent_1),
        Event('delegated', parent_1),
        Event('received', child_1),
        Event('completed', parent_1),
        Event('info', child_1),
    ], {'completed': 1, 'running': 1, 'pending': 0, 'failed': 0})

    # No delegate complete | 1 complete, 1 running, 0 pending, 0 failed
    scenario_5 = ('No delegate complete', [
        Event('started', parent_1),
        Event('info', parent_1),
        Event('delegated', parent_1),
        Event('received', child_1),
        Event('info', child_1),
        Event('completed', child_1)
    ], {'completed': 1, 'running': 1, 'pending': 0, 'failed': 0})

    # Receive before delegate no delegate completed
    # | 1 complete, 0 pending, 1 running, 0 failed
    scenario_6 = ('Receive before delegate no delegate completed', [
        Event('started', parent_1),
        Event('info', parent_1),
        Event('received', child_1),
        Event('delegated', parent_1),
        Event('info', child_1),
        Event('completed', child_1)
    ], {'completed': 1, 'running': 1, 'pending': 0, 'failed': 0})

    # Receive before delegate no received completed
    # | 1 complete, 0 pending, 1 running,  0 failed
    scenario_7 = ('Receive before delegate no received completed', [
        Event('started', parent_1),
        Event('info', parent_1),
        Event('received', child_1),
        Event('delegated', parent_1),
        Event('completed', parent_1),
        Event('info', child_1),
    ], {'completed': 1, 'running': 1, 'pending': 0, 'failed': 0})

    # No completes | 0 complete, 0 pending, 2 running, 0 failed
    scenario_8 = ('No completes', [
        Event('started', parent_1),
        Event('info', parent_1),
        Event('delegated', parent_1),
        Event('received', child_1),
        Event('info', child_1),
    ], {'completed': 0, 'running': 2, 'pending': 0, 'failed': 0})

    # No delegate | 1 complete, 0 pending, 1 running, 0 failed
    scenario_9 = ('No delegate', [
        Event('started', parent_1),
        Event('info', parent_1),
        Event('received', child_1),
        Event('info', child_1),
        Event('completed', child_1)
    ], {'completed': 1, 'running': 1, 'pending': 0, 'failed': 0})

    return (scenario_1, scenario_2, scenario_3,
            scenario_4, scenario_5, scenario_6,
            scenario_7, scenario_8, scenario_9,)


def test_task_states(task_scenarios):

    for scenario in task_scenarios:
        results = PiperCiResource.task_states(None, scenario[1])

        for state, value in scenario[2].items():
            assert value == len(results[state])


def test_task_states_expected_types(task_scenarios):
    for scenario in task_scenarios:
        results = PiperCiResource.task_states(None, scenario[1])

        for state, tasks in results.items():
            assert isinstance(tasks, list), (
                'Invalid returned iterable type of {type(tasks)} should be: list')

            for task in tasks:
                assert isinstance(task, Task), 'Non Task returned in list of tasks'
