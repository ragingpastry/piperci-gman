## [1.3.1](https://gitlab.com/dreamer-labs/piperci/piperci-gman/compare/v1.3.0...v1.3.1) (2019-11-21)


### Bug Fixes

* Upgraded to marshmallow 3.x + improved input validation ([69410e5](https://gitlab.com/dreamer-labs/piperci/piperci-gman/commit/69410e5))

# [1.3.0](https://gitlab.com/dreamer-labs/piperci/piperci-gman/compare/v1.2.0...v1.3.0) (2019-11-21)


### Bug Fixes

* pinning Marshmallow-Peewee till we can resolve conflicts ([383a053](https://gitlab.com/dreamer-labs/piperci/piperci-gman/commit/383a053))


### Features

* Added health check URI ([1e22ef3](https://gitlab.com/dreamer-labs/piperci/piperci-gman/commit/1e22ef3))

# [1.2.0](https://gitlab.com/dreamer-labs/piperci/piperci-gman/compare/v1.1.0...v1.2.0) (2019-10-17)


### Bug Fixes

* test_app was writing to disk gman.db... naughty ([8bb2c71](https://gitlab.com/dreamer-labs/piperci/piperci-gman/commit/8bb2c71))


### Features

* Added filter_by support for artifacts ([b2448a0](https://gitlab.com/dreamer-labs/piperci/piperci-gman/commit/b2448a0))
* record artifacts used by tasks + filter support for artifacts ([7101871](https://gitlab.com/dreamer-labs/piperci/piperci-gman/commit/7101871))
* Updated openapi.yml for new API features + added openapi lint ([45e7989](https://gitlab.com/dreamer-labs/piperci/piperci-gman/commit/45e7989))

# [1.1.0](https://gitlab.com/dreamer-labs/piperci/piperci-gman/compare/v1.0.0...v1.1.0) (2019-10-07)


### Features

* Added stdout, stderr to artifact types ([d5c20bd](https://gitlab.com/dreamer-labs/piperci/piperci-gman/commit/d5c20bd))

# 1.0.0 (2019-08-16)


### Bug Fixes

* add entrypoint to force perms ([02102eb](https://gitlab.com/dreamer-labs/piperci/piperci-gman/commit/02102eb))
