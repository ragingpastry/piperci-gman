# GMan

A sinister inter-dimensional bureaucrat that monitors the state of each FaaS.

## Install dev environment

`pip install -e .`

## Run dev server

`python index.py`

## Testing

`tox -e unittest`

## Notes About API

- Dates: timestamp is an iso8601 formatted date
- urlsafe SubResource Integrity(SRI) hashes are used for sri values not raw shaw256-myhashvalue
